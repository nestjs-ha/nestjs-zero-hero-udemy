import { Repository, EntityRepository } from 'typeorm';
import * as Bcrypt from 'bcrypt';
import { User } from './user.entity';
import { AuthCredentialsDto } from './dto/auth-credentials.dto';
import { ConflictException, InternalServerErrorException } from '@nestjs/common';

@EntityRepository(User)
export class UserRepository extends Repository<User> {
    async signUp(authCredentialsDto: AuthCredentialsDto ): Promise<void> {
        const { username, password } = authCredentialsDto;

        const user = new User();
        user.username = username;
        user.salt = await Bcrypt.genSalt();
        console.log('Salt= ', user.salt);
        // uses generated salt + text password to encrypt the password
        // makes it harder to decrypt password, if db is breached
        // this salt is unique per user
        user.password = await this.hashPassword( password, user.salt);

        // allows you to check for duplicate users, using one db query
        try {
            await user.save();
        } catch ( error ) {
            if (error.code === '23505') { // duplicate username error
                throw new ConflictException('Username already exists!');
            } else {
                throw new InternalServerErrorException();
            }
        }
    }

    async validateUserPassword(authCredentialsDto: AuthCredentialsDto): Promise<string> {
        const { username, password } = authCredentialsDto;
        const user = await this.findOne({username});

        // use custom entity method to validate password
        if ( user && await user.validatePassword(password)) {
            return  user.username;
        } else {
            return null;
        }

    }

    private async hashPassword( password: string, salt: string): Promise<string> {
        return Bcrypt.hash( password, salt);
    }
}
